<?php

use Workerman\Worker;

require_once __DIR__ . '/../vendor/autoload.php';

$wsWorker = new Worker('websocket://0.0.0.0:2346');

// Количество процесов, которое будет обрабатываться от клиентов
$wsWorker->count = 6;

// Обработка подключения / Этот колбэк вызывается сразу после подключения пользователя
$wsWorker->onConnect = function ($connection) {
    echo "Новое соединение \n";
};

// Обработка сообщений от пользователя
$wsWorker->onMessage = function ($connection, $data) use ($wsWorker) {
    var_dump($data);
    // Пробрасываем сообщение всем пользователям
    foreach($wsWorker->connections as $clientConnection) {
        $clientConnection->send($data);
    }
};

// Обработка отключения / Этот колбэк вызывается сразу после одключения пользователя
$wsWorker->onClose = function ($connection) {
    echo "Соединение закрыто \n";
};

// Запускаем наш сервер
Worker::runAll();