# Проект на WebSocket

### Команды

- Установить http-server  
  `npm i -g http-server`
  

- Запустить http-server  
  `http-server client`


- Установить workerman фреймворк для WebSocket  
  `composer require workerman/workerman`


- Запустить workerman фреймворк для WebSocket  
  `php server/websocket.php start -d`