window.addEventListener('load', () => {

    const wrapper = document.getElementById('wrapper')
    const unit = document.getElementById('unit')
    unit.style.backgroundColor = Math.random() < 0.5 ? 'red' : 'green'

    // Открываем webSocket соединение
    const ws = new WebSocket('ws://0.0.0.0:2346')

    // Обработчик события клика
    wrapper.addEventListener('keyup', e => {
        let top = unit.style.top ? unit.style.top : 0
        let left = unit.style.left ? unit.style.left : 0
        const step = 5

        // Смещаем unit
        e.code == 'ArrowUp' ? unit.style.top = parseInt(top) - step + 'px' : ''
        e.code == 'ArrowDown' ? unit.style.top = parseInt(top) + step + 'px' : ''
        e.code == 'ArrowLeft' ? unit.style.left = parseInt(left) - step + 'px' : ''
        e.code == 'ArrowRight' ? unit.style.left = parseInt(left) + step + 'px' : ''

        // Отправляем на сервер данные по позиции unit
        let positionData = {
            top: unit.style.top,
            left: unit.style.left
        }

        // Отправляем на сервер
        ws.send(JSON.stringify(positionData))
    })

    // Приём от сервера
    ws.onmessage = response => {
        let positionData = JSON.parse(response.data)
        console.log(positionData)
        unit.style.top = positionData.top
        unit.style.left = positionData.left
    }
})